package fr.umfds;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class JeuTest {

    @Mock
    Dico mockDico;
    Jeu jeu;

    public JeuTest() throws IOException {
    }

    @org.junit.jupiter.api.Test
    void choixMotTest() throws IOException {


        when(mockDico.randomWord(5)).thenReturn("aa-aa").thenReturn("aaaaa");
        jeu = new Jeu(mockDico);

        assertTrue(jeu.choixMot(5).equals("aaaaa"));


    }

    @org.junit.jupiter.api.Test
    void analyseTestMotLongueurIncorrecte() throws IOException {

        when(mockDico.randomWord(3)).thenReturn("mot");



        jeu = new Jeu(mockDico);
        assertEquals("mot", jeu.choixMot(3));

        assertTrue(jeu.analyse("chaineBcpTropLongue").contains("incorrecte"));

        assertEquals("m_t", jeu.analyse("mat"));

        assertEquals("mot", jeu.analyse("mot"));


    }
}
