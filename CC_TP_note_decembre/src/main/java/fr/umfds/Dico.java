package fr.umfds;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Stream;

public class Dico {
    private Path path= Paths.get("src/main/resource/listeMots.txt");
    private HashMap<Integer, ArrayList<String>> mots=new HashMap<>();

    public Dico() throws IOException {

        BufferedReader reader = Files.newBufferedReader(path);

        Stream<String> stream = Files.lines(path);
        stream.forEach((String mot)->{
            int size=mot.length();
            if (mots.containsKey(size)){
                mots.get(size).add(mot);
            }else{
                ArrayList<String> l=new ArrayList<>();
                l.add(mot);
                mots.put(size, l);
            }
        });
    }

    public String randomWord(int size){
        Random r=new Random(System.currentTimeMillis());
        int tailleListeMots=mots.get(size).size();
        int alea=r.nextInt(tailleListeMots);
        return mots.get(size).get(alea);
    }
}
